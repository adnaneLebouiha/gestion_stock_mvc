package com.lebouiha.gestionstock.services.impl;

import java.io.InputStream;

import com.lebouiha.gestionstock.dao.IFlickrDao;
import com.lebouiha.gestionstock.services.IFlickrService;

public class FlickrServiceImpl implements IFlickrService {

	
	
	private IFlickrDao dao; 
	
	@Override
	public String savePhoto(InputStream photo, String title) throws Exception {
		// TODO Auto-generated method stub
		return dao.savePhoto(photo, title);
	}

	public void setDao(IFlickrDao dao) {
		this.dao = dao;
	}

	
	
}
