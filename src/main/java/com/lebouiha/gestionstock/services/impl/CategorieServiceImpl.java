package com.lebouiha.gestionstock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.lebouiha.gestionstock.dao.ICategorieDao;
import com.lebouiha.gestionstock.entities.Categorie;
import com.lebouiha.gestionstock.services.ICategorieService;

@Transactional
public class CategorieServiceImpl implements ICategorieService{

	private ICategorieDao dao;
	
	@Override
	public Categorie save(Categorie entity) {
		// TODO Auto-generated method stub
		return dao.save(entity);
	}

	@Override
	public Categorie update(Categorie entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public List<Categorie> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	@Override
	public List<Categorie> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Categorie getById(Long id) {
		// TODO Auto-generated method stub
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		dao.remove(id);
	}

	@Override
	public Categorie findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Categorie findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findOne(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(paramName, paramValue);
	}

	public void setDao(ICategorieDao dao) {
		this.dao = dao;
	}

}
