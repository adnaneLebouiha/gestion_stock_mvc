package com.lebouiha.gestionstock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.lebouiha.gestionstock.dao.ICmdClientDao;
import com.lebouiha.gestionstock.entities.CmdClient;
import com.lebouiha.gestionstock.services.ICmdClientService;

@Transactional
public class CmdClientServiceImpl implements ICmdClientService{

	private ICmdClientDao dao;
	
	@Override
	public CmdClient save(CmdClient entity) {
		// TODO Auto-generated method stub
		return dao.save(entity);
	}

	@Override
	public CmdClient update(CmdClient entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public List<CmdClient> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	@Override
	public List<CmdClient> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortField, sort);
	}

	@Override
	public CmdClient getById(Long id) {
		// TODO Auto-generated method stub
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		dao.remove(id);
	}

	@Override
	public CmdClient findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public CmdClient findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findOne(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(paramName,paramValue);
	}

	public void setDao(ICmdClientDao dao) {
		this.dao = dao;
	}

}
