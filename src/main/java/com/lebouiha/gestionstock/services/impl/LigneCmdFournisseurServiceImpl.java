package com.lebouiha.gestionstock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.lebouiha.gestionstock.dao.ILigneCmdFournisseurDao;
import com.lebouiha.gestionstock.entities.LigneCmdFournisseur;
import com.lebouiha.gestionstock.services.ILigneCmdFournisseurService;

@Transactional
public class LigneCmdFournisseurServiceImpl implements ILigneCmdFournisseurService{

	private ILigneCmdFournisseurDao dao;
	
	@Override
	public LigneCmdFournisseur save(LigneCmdFournisseur entity) {
		// TODO Auto-generated method stub
		return dao.save(entity);
	}

	@Override
	public LigneCmdFournisseur update(LigneCmdFournisseur entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public List<LigneCmdFournisseur> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	@Override
	public List<LigneCmdFournisseur> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortField, sort);
	}

	@Override
	public LigneCmdFournisseur getById(Long id) {
		// TODO Auto-generated method stub
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		dao.remove(id);
	}

	@Override
	public LigneCmdFournisseur findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public LigneCmdFournisseur findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findOne(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(paramName, paramValue);
	}

	public void setDao(ILigneCmdFournisseurDao dao) {
		this.dao = dao;
	}
}
