package com.lebouiha.gestionstock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.lebouiha.gestionstock.dao.IMvtStockDao;
import com.lebouiha.gestionstock.entities.MvtStock;
import com.lebouiha.gestionstock.services.IMvtStockService;

@Transactional
public class MvtStockServiceImpl implements IMvtStockService{

	private IMvtStockDao dao;
	
	@Override
	public MvtStock save(MvtStock entity) {
		// TODO Auto-generated method stub
		return dao.save(entity);
	}

	@Override
	public MvtStock update(MvtStock entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public List<MvtStock> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	@Override
	public List<MvtStock> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortField, sort);
	}

	@Override
	public MvtStock getById(Long id) {
		// TODO Auto-generated method stub
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		dao.remove(id);
	}

	@Override
	public MvtStock findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public MvtStock findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findOne(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(paramName, paramValue);
	}

	public void setDao(IMvtStockDao dao) {
		this.dao = dao;
	}
	
}
