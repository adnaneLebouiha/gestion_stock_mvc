package com.lebouiha.gestionstock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.lebouiha.gestionstock.dao.ILigneCmdClientDao;
import com.lebouiha.gestionstock.entities.LigneCmdClient;
import com.lebouiha.gestionstock.services.ILigneCmdClientService;

@Transactional
public class LigneCmdClientServiceImpl implements ILigneCmdClientService {
	
	private ILigneCmdClientDao dao;
	
	@Override
	public LigneCmdClient save(LigneCmdClient entity) {
		// TODO Auto-generated method stub
		return dao.save(entity);
	}

	@Override
	public LigneCmdClient update(LigneCmdClient entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public List<LigneCmdClient> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	@Override
	public List<LigneCmdClient> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortField, sort);
	}

	@Override
	public LigneCmdClient getById(Long id) {
		// TODO Auto-generated method stub
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		dao.remove(id);
	}

	@Override
	public LigneCmdClient findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public LigneCmdClient findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findOne(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(paramName, paramValue);
	}

	public void setDao(ILigneCmdClientDao dao) {
		this.dao = dao;
	}

}
