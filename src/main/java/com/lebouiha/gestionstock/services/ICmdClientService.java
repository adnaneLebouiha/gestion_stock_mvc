package com.lebouiha.gestionstock.services;

import java.util.List;

import com.lebouiha.gestionstock.entities.CmdClient;

public interface ICmdClientService {
	
	public CmdClient save(CmdClient entity);
	
	public CmdClient update(CmdClient entity);
		
	public List<CmdClient> selectAll();
		
	public List<CmdClient> selectAll(String sortField, String sort);
		
	public CmdClient getById(Long id);
		
	public void remove(Long id);
		
	public CmdClient findOne(String paramName, Object paramValue);
		
	public CmdClient findOne(String[] paramNames, Object[] paramValues);
		
	public int findOne(String paramName, String paramValue);
}
