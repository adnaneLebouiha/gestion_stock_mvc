package com.lebouiha.gestionstock.services;

import java.util.List;

import com.lebouiha.gestionstock.entities.CmdFournisseur;

public interface ICmdFournisseurService {

	public CmdFournisseur save(CmdFournisseur entity);
	
	public CmdFournisseur update(CmdFournisseur entity);
		
	public List<CmdFournisseur> selectAll();
		
	public List<CmdFournisseur> selectAll(String sortField, String sort);
		
	public CmdFournisseur getById(Long id);
		
	public void remove(Long id);
		
	public CmdFournisseur findOne(String paramName, Object paramValue);
		
	public CmdFournisseur findOne(String[] paramNames, Object[] paramValues);
		
	public int findOne(String paramName, String paramValue);
}
