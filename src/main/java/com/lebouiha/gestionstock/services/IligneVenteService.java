package com.lebouiha.gestionstock.services;

import java.util.List;

import com.lebouiha.gestionstock.entities.LigneVente;

public interface IligneVenteService {

	public LigneVente save(LigneVente entity);
	
	public LigneVente update(LigneVente entity);
		
	public List<LigneVente> selectAll();
		
	public List<LigneVente> selectAll(String sortField, String sort);
		
	public LigneVente getById(Long id);
		
	public void remove(Long id);
		
	public LigneVente findOne(String paramName, Object paramValue);
		
	public LigneVente findOne(String[] paramNames, Object[] paramValues);
		
	public int findOne(String paramName, String paramValue);
}
