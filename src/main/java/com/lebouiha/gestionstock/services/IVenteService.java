package com.lebouiha.gestionstock.services;

import java.util.List;

import com.lebouiha.gestionstock.entities.Vente;

public interface IVenteService {

	public Vente save(Vente entity);
	
	public Vente update(Vente entity);
		
	public List<Vente> selectAll();
		
	public List<Vente> selectAll(String sortField, String sort);
		
	public Vente getById(Long id);
		
	public void remove(Long id);
		
	public Vente findOne(String paramName, Object paramValue);
		
	public Vente findOne(String[] paramNames, Object[] paramValues);
		
	public int findOne(String paramName, String paramValue);
}
