package com.lebouiha.gestionstock.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.lebouiha.gestionstock.entities.Article;
import com.lebouiha.gestionstock.entities.Categorie;
import com.lebouiha.gestionstock.services.IArticleService;
import com.lebouiha.gestionstock.services.ICategorieService;
import com.lebouiha.gestionstock.services.IFlickrService;

/**
 * @author Lebouiha
 * .
 *
 */
@Controller
@RequestMapping(value = "/article")
public class ArticleController {

	@Autowired
	private IArticleService articleService;
	
	@Autowired
	private IFlickrService flickerService;
	
	@Autowired
	private ICategorieService categorieService;
	
	
	@RequestMapping(value = "/")
	public String article(Model model) {
		
		List<Article> articles = articleService.selectAll();
		if(articles == null) articles = new ArrayList<Article>();		
		
		
		model.addAttribute("articles", articles);
		return "article/article";
	}
	
	
	@RequestMapping(value = "/nouveau",method = RequestMethod.GET)
	public String ajouterArticle(Model model) {
		
		Article article = new Article();
		model.addAttribute("article",article);
		
		List<Categorie> categories = categorieService.selectAll();
		if(categories == null) categories = new ArrayList<Categorie>(); 
		model.addAttribute("categories",categories);
		
		return "article/ajouterArticle";
	}
	
	
	@RequestMapping(value = "/enregistrer",method = RequestMethod.POST)
	public String enregistrerArticle(Model model, Article article, MultipartFile file){
		String photoUrl = null;
	 if(article !=null) {
		if(file!= null && !file.isEmpty()) {				
			InputStream stream = null;
			try {
				stream = file.getInputStream();
				photoUrl = flickerService.savePhoto(stream, article.getCodeArticle());
				article.setPhoto(photoUrl);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					stream.close();
				} catch (IOException e) {
					
					e.printStackTrace();
				}
			}													
		}
		if(article.getIdArticle()!=null) {
		articleService.update(article);	
		}else {
		articleService.save(article);
		}
	}
	 			
		return "redirect:/article/";
	}
	
	@RequestMapping(value = "/modifier/{idArticle}")
	public String modifierArticle(Model model,@PathVariable Long idArticle) {
		if(idArticle != null) {
		
			Article article = articleService.getById(idArticle);
			if(article != null) {
				model.addAttribute("article", article);
			}
		}
	 	
		
		return "article/ajouterArticle";
	}
	
	@RequestMapping(value="/supprimer/{idArticle}")
	public String supprimerArticle(Model model, @PathVariable Long idArticle) {
		
		Article article = articleService.getById(idArticle);
		if(article!=null) {
			if(idArticle != null) {
				//TODO ajouter les controles m�tier
			articleService.remove(idArticle);
			}
		}
		
		return "redirect:/article/";
	}
	
}
