package com.lebouiha.gestionstock.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.lebouiha.gestionstock.entities.Categorie;
import com.lebouiha.gestionstock.services.ICategorieService;

/**
 * @author Lebouiha
 *
 */
@Controller
@RequestMapping(value = "/categorie")
public class CategorieController {

	@Autowired
	private ICategorieService categorieService;
	
	
	@RequestMapping(value = "/")
	public String categorie(Model model) {
		
		List<Categorie> categories = categorieService.selectAll();
		if(categories == null) categories = new ArrayList<Categorie>();
		
		
		model.addAttribute("categories", categories);
		return "categorie/categorie";
	}
	
	
	@RequestMapping(value = "/nouveau",method = RequestMethod.GET)
	public String ajouterCategorie(Model model) {
		
		//Client client = new Client();
		Categorie categorie = new Categorie();
		model.addAttribute("categorie",categorie);
		
		return "categorie/ajouterCategorie";
	}
	
	
	@RequestMapping(value = "/enregistrer",method = RequestMethod.POST)
	public String enregistrerCategorie(Model model,Categorie categorie){	
	 
		if(categorie.getIdCategorie() != null) {		
		categorieService.update(categorie);
		}else {
		categorieService.save(categorie);
		}		 		
		return "redirect:/categorie/";
	}
	
	@RequestMapping(value = "/modifier/{idCategorie}")
	public String modifierClient(Model model,@PathVariable Long idCategorie) {
		if(idCategorie != null) {					
			Categorie categorie = categorieService.getById(idCategorie);
			if(categorie != null) {
				model.addAttribute("categorie", categorie);
			}
		}
	 			
		return "categorie/ajouterCategorie";
	}
	
	@RequestMapping(value="/supprimer/{idCategorie}")
	public String supprimerClient(Model model, @PathVariable Long idCategorie) {		
		Categorie categorie = categorieService.getById(idCategorie);
		if(categorie!=null) {
			if(idCategorie != null) {
				//TODO ajouter les controles m�tier
				categorieService.remove(idCategorie);
			}
		}
		
		return "redirect:/categorie/";
	}
	
}
