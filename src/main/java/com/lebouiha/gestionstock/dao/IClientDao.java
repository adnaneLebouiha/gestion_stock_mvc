package com.lebouiha.gestionstock.dao;

import com.lebouiha.gestionstock.entities.Client;

public interface IClientDao extends IGeneriqueDao<Client> {

}
