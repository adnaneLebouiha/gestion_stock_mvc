package com.lebouiha.gestionstock.dao;

import com.lebouiha.gestionstock.entities.Utilisateur;

public interface IUtilisateurDao extends IGeneriqueDao<Utilisateur>{

}
