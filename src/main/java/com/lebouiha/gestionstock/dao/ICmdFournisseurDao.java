package com.lebouiha.gestionstock.dao;

import com.lebouiha.gestionstock.entities.CmdFournisseur;

public interface ICmdFournisseurDao extends IGeneriqueDao<CmdFournisseur> {

}
