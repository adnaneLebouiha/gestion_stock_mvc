package com.lebouiha.gestionstock.dao;

import com.lebouiha.gestionstock.entities.Fournisseur;

public interface IFournisseurDao extends IGeneriqueDao<Fournisseur>{

}
