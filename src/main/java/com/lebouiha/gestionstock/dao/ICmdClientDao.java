package com.lebouiha.gestionstock.dao;

import com.lebouiha.gestionstock.entities.CmdClient;

public interface ICmdClientDao extends IGeneriqueDao<CmdClient> {

}
