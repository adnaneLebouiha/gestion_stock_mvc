package com.lebouiha.gestionstock.dao;

import com.lebouiha.gestionstock.entities.LigneVente;

public interface ILigneVenteDao extends IGeneriqueDao<LigneVente>{

}
