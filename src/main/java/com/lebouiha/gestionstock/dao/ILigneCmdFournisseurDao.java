package com.lebouiha.gestionstock.dao;

import com.lebouiha.gestionstock.entities.LigneCmdFournisseur;

public interface ILigneCmdFournisseurDao extends IGeneriqueDao<LigneCmdFournisseur>{

}
