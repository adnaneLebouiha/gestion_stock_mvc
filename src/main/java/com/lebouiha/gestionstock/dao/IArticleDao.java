package com.lebouiha.gestionstock.dao;

import com.lebouiha.gestionstock.entities.Article;

public interface IArticleDao extends IGeneriqueDao<Article> {

}
