package com.lebouiha.gestionstock.dao;

import com.lebouiha.gestionstock.entities.Categorie;

public interface ICategorieDao extends IGeneriqueDao<Categorie> {

}
