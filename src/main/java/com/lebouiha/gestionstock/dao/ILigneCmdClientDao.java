package com.lebouiha.gestionstock.dao;

import com.lebouiha.gestionstock.entities.LigneCmdClient;

public interface ILigneCmdClientDao extends IGeneriqueDao<LigneCmdClient> {

}
