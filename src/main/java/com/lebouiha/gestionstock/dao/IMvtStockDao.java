package com.lebouiha.gestionstock.dao;

import com.lebouiha.gestionstock.entities.MvtStock;

public interface IMvtStockDao extends IGeneriqueDao<MvtStock>{

}
